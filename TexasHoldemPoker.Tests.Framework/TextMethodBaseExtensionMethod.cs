﻿using System;
using System.Reflection;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Tests.Framework
{
    public static class TextMethodBaseExtensionMethod
    {
        public static string NameAsReadableSentence(this MethodBase method)
        {
            return $"{ReadableTypeName(method.ReflectedType)} {ReadableMethodName(method)}";
        }

        internal static string ReadableTypeName(Type type)
        {
            return string.Join(" ", type.Name.SplitOnUpperCaseLetters());
        }

        internal static string ReadableMethodName(MethodBase method)
        {
            return method.Name.ToLowerInvariant().Replace("_", " ");
        }


    }
}
