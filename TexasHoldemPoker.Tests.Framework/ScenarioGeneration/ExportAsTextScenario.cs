﻿using System;
using System.Collections.Generic;

namespace TexasHoldemPoker.Tests.Framework.ScenarioGeneration
{

    class ExportAsText : IExportAScenario
    {
        private readonly List<string> allLines = new List<string>();

        private void AddLines(IEnumerable<string> lines)
        {
            allLines.AddRange(lines);
        }

        private void AddLine(string line)
        {
            allLines.Add(line);
        }

        public IExportAScenario WriteGiven(IEnumerable<Step> givenSteps)
        {
            AddLines(givenSteps.WrittenAs("GIVEN"));
            return this;
        }

        public IExportAScenario WriteName(string name)
        {
            allLines.Add($"Scenario : {name}");
            return this;
        }

        public IExportAScenario WriteThen(IEnumerable<Step> thenSteps)
        {
            AddLines(thenSteps.WrittenAs("THEN"));
            return this;
        }

        public IExportAScenario WriteWhen(Step step)
        {
            AddLine(step.WrittenAs("WHEN"));
            return this;
        }
        

        public string FullText => string.Join(Environment.NewLine, allLines);
    }

}
