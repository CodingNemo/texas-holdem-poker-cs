﻿
using System.Collections.Generic;
using System.Linq;

namespace TexasHoldemPoker.Tests.Framework.ScenarioGeneration
{
    public static class StepExtensionMethods
    {
        public static IEnumerable<string> WrittenAs(this IEnumerable<Step> steps, string stepType)
        {
            if (!steps.Any())
            {
                yield break;
            }

            yield return steps.First().WrittenAs(stepType);

            if (steps.Count() > 1)
            {
                string WriteAndStep(Step s)
                {
                    var and = "And".PadLeft(stepType.Length);
                    return s.WrittenAs(and);
                }

                foreach (var step in steps.Skip(1))
                {
                    yield return WriteAndStep(step);
                }
            }
        }

        public static string WrittenAs(this Step step, string stepType)
        {
            string stepAsString = step.ToString();
            return $"{stepType} {stepAsString}".PadLeft(3 + stepType.Length + stepAsString.Length + 1);
        }
    }
}
