﻿using System.Collections.Generic;

namespace TexasHoldemPoker.Tests.Framework.ScenarioGeneration
{
    public class Scenario
    {
        public string ScenarioName { get; } = "";

        internal IEnumerable<Step> GivenSteps => givenSteps;
        private readonly List<Step> givenSteps = new List<Step>();

        internal Step WhenStep { get; private set; }

        internal IEnumerable<Step> ThenSteps => thenSteps;

        private readonly List<Step> thenSteps = new List<Step>();
        private readonly IEnumerable<IExportAScenario> exporters;

        public Scenario(string scenarioName, IEnumerable<IExportAScenario> exporters)
        {
            this.exporters = exporters;

            ScenarioName = scenarioName;
        }

        public Scenario Given(Step step)
        {
            givenSteps.Add(step);
            return this;
        }

        public Scenario WithHistory(IEnumerable<Step> given)
        {
            givenSteps.AddRange(given);
            return this;
        }

        public Scenario When(Step step)
        {
            WhenStep = step;
            return this;
        }

        public Scenario Then(params Step[] steps)
        {
            return Then((IEnumerable<Step>)steps);
        }

        public Scenario Then(IEnumerable<Step> steps)
        {
            thenSteps.AddRange(steps);
            return this;
        }

        public void Export()
        {
            foreach (var exporter in exporters)
            {
                exporter
                    .WriteName(ScenarioName)
                    .WriteGiven(GivenSteps)
                    .WriteWhen(WhenStep)
                    .WriteThen(ThenSteps);
            }
        }
    }
}
