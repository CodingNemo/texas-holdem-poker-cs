﻿using System.Collections.Generic;

namespace TexasHoldemPoker.Tests.Framework.ScenarioGeneration
{
    public interface IExportAScenario
    {
        IExportAScenario WriteName(string name);
        IExportAScenario WriteGiven(IEnumerable<Step> givenSteps);
        IExportAScenario WriteWhen(Step whenStep);
        IExportAScenario WriteThen(IEnumerable<Step> thenSteps);
    }
}
