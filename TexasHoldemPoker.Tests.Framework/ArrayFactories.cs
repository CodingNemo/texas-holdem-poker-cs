namespace TexasHoldemPoker.Tests.Framework
{
    public static class ArrayFactories
    {
        public static T[] A<T>(T item)
        {
            return new[]
            {
                item
            };
        }

        public static T[] Many<T>(params T[] array)
        {
            return array;
        }
    }
}