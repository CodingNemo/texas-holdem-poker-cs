﻿using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Tables.Commands
{
    public class OpenNewTable : New<Table>
    {
        public TableId TableId { get; }

        public int NumberOfSeats { get; }
        public decimal SmallBlindAmount { get; }
        public decimal BigBlindAmount { get; }

        public object AggregateId => TableId;

        public OpenNewTable(
            TableId tableId, int numberOfSeats, 
            decimal smallBlindAmount, decimal bigBlindAmount)
        {
            TableId = tableId;
            NumberOfSeats = numberOfSeats;
            SmallBlindAmount = smallBlindAmount;
            BigBlindAmount = bigBlindAmount;
        }
    }
}