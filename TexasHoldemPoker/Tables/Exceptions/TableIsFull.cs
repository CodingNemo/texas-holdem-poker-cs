﻿using System;
using System.Runtime.Serialization;

namespace TexasHoldemPoker.Tables.Exceptions
{
    [Serializable]
    public class TableIsFull : Exception
    {
        public TableIsFull(Exception innerException = null)
            : base("Nobody can seats to the table anymore", innerException)
        {
        }

        protected TableIsFull(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}