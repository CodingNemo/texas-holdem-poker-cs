﻿using TexasHoldemPoker.Players;

namespace TexasHoldemPoker.Tables
{
    public class SeatedPlayer
    {
        public SeatedPlayer(PlayerId player, decimal stake)
        {
            PlayerId = player;
            Stake = stake;
        }

        public PlayerId PlayerId { get; }
        public decimal Stake { get; set; }

        public override string ToString()
        {
            return $"{PlayerId} (Stack : {Stake})";
        }
    }
}