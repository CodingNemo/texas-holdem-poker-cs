﻿using TexasHoldemPoker.Cards;

namespace TexasHoldemPoker.Hands.HandValues
{
    public class Flush : HandValue
    {
        private const long FLUSH_VALUE = 5000000;

        public Flush(Hand hand, Suit suit)
            : base(hand)
        {
            Suit = suit;
        }

        public Suit Suit { get; }

        internal override long ValueStrength => FLUSH_VALUE;

        protected override long ComputeHandStrength()
        {
            return (long) Suit * 5 + (long) Hand.Last.Rank;
        }

        public override string ToString()
        {
            return base.ToString() + $" of {Suit}";
        }
    }
}