﻿using TexasHoldemPoker.Cards;

namespace TexasHoldemPoker.Hands.HandValues
{
    public class Pair : CardGroupsHandValue
    {
        private const long PAIR_VALUE = 1000000;

        public Pair(Hand hand, Rank rank, HighCard remainingCards)
            : base(hand, rank, remainingCards, 2) { }

        internal override long ValueStrength => PAIR_VALUE;
    }
}