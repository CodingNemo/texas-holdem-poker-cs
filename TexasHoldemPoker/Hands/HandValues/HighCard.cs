﻿using System;
using TexasHoldemPoker.Cards;

namespace TexasHoldemPoker.Hands.HandValues
{
    public class HighCard : HandValue
    {
        public HighCard(Hand hand, params Card[] cards)
            : base(hand)
        {
            Cards = cards.Length == 0 ? null : cards;
        }

        public Card[] Cards { get; }

        internal override long ValueStrength => 0;

        protected override long ComputeHandStrength()
        {
            var index = 0;
            long strength = 0;

            var cardCount = Cards?.Length ?? Hand.Size;

            for (var cardIndex = 0; cardIndex < cardCount; cardIndex++)
            {
                var card = Cards != null ? Cards[cardIndex] : Hand[cardIndex];
                var rankAsLong = (long) card.Rank;
                var factor = (long) Math.Pow(14, index++);
                strength += rankAsLong * factor;
            }

            return strength;
        }
    }
}