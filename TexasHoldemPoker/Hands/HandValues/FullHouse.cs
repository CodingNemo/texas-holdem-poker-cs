﻿namespace TexasHoldemPoker.Hands.HandValues
{
    public class FullHouse : HandValue
    {
        private const long FULLHOUSE_VALUE = 6000000;

        public FullHouse(Hand hand, ThreeOfAKind threeOfAKind, Pair pair)
            : base(hand)
        {
            ThreeOfAKind = threeOfAKind;
            Pair = pair;
        }

        public ThreeOfAKind ThreeOfAKind { get; }
        public Pair Pair { get; }

        internal override long ValueStrength => FULLHOUSE_VALUE;

        protected override long ComputeHandStrength()
        {
            return (long) ThreeOfAKind.Rank * 14 + (long) Pair.Rank;
        }

        public override string ToString()
        {
            return base.ToString() + $" with {ThreeOfAKind.Rank} over {Pair.Rank}";
        }
    }
}