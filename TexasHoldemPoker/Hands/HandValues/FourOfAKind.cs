﻿using TexasHoldemPoker.Cards;

namespace TexasHoldemPoker.Hands.HandValues
{
    public class FourOfAKind : CardGroupsHandValue
    {
        private const long FOUR_OF_A_KIND_VALUE = 7000000;

        public FourOfAKind(
            Hand hand,
            Rank rank,
            HighCard remainingCard)
            : base(hand, rank, remainingCard, 4) { }
        
        internal override long ValueStrength => FOUR_OF_A_KIND_VALUE;
    }
}