﻿using System;
using System.Linq;
using TexasHoldemPoker.Cards;

namespace TexasHoldemPoker.Hands
{
    public class Hand : IEquatable<Hand>
    {
        private readonly Card[] cards;

        public Hand(int handSize)
        {
            cards = new Card[handSize];
            Size = 0;
        }

        public int Size { get; private set; }

        public Card First
        {
            get
            {
                if (Size <= 0)
                {
                    throw new InvalidOperationException("Hand is Empty");
                }

                return cards[0];
            }
        }

        public Card Last
        {
            get
            {
                if (Size <= 0)
                {
                    throw new InvalidOperationException("Hand is Empty");
                }

                return cards[Size - 1];
            }
        }

        public Card this[int i] => cards[i];

        public bool Equals(Hand other)
        {
            return other != null && cards.SequenceEqual(other.cards);
        }

        public bool Contains(Card card)
        {
            return Array.IndexOf(cards, card) > -1;
        }

        public Hand AddCard(Card card)
        {
            if (Contains(card))
            {
                throw new Exception($"Hand {this} already has a card {card}");
            }

            if (Size >= cards.Length)
            {
                throw new Exception($"Hand {this} is full. Can not add {card}");
            }

            Append(card);
            return this;
        }

        public void Append(Card card)
        {
            var insertedCard = false;

            if (Size > 0)
            {
                var previousIndex = Size - 1;

                while (previousIndex >= 0)
                {
                    var previousCard = cards[previousIndex];

                    // Already ordered
                    if (previousCard.Rank < card.Rank)
                    {
                        break;
                    }

                    // Already ordered
                    if (previousCard.Rank == card.Rank
                        && previousCard.Suit < card.Suit)
                    {
                        break;
                    }

                    InsertCard(previousIndex + 1, previousCard);
                    InsertCard(previousIndex, card);

                    insertedCard = true;

                    previousIndex--;
                }

                if (insertedCard)
                {
                    Size++;
                }
            }

            if (!insertedCard) AddCardToTheEnd(card);
        }

        private void AddCardToTheEnd(Card card)
        {
            InsertCard(Size++, card);
        }

        private void InsertCard(int position, Card card)
        {
            cards[position] = card;
        }
        
        public override int GetHashCode()
        {
            unchecked
            {
                var hash = 17;

                for (var cardIndex = 0; cardIndex < Size; cardIndex++)
                {
                    var card = cards[cardIndex];
                    hash = hash * 23 + card.GetHashCode();
                }

                return hash;
            }
        }

        public override bool Equals(object obj)
        {
            return Equals((Hand) obj);
        }

        public override string ToString()
        {
            return string.Join(" ", (object[]) cards);
        }
    }
}