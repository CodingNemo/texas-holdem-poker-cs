﻿using System.Collections.Generic;
using System.Linq;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Hands.HandValues;

namespace TexasHoldemPoker.Hands
{
    public class HandEvaluator
    {
        public HandValue FindBestHand(List<Card> cards)
        {
            if (cards.Count <= 5)
            {
                var hand = new Hand(5);
                cards.ToList().ForEach(c => hand.AddCard(c));
                return Evaluate(hand);
            }

            var evaluations = cards.CombinationsOf(5).ToList()
                .Select(combination =>
                {
                    var hand = new Hand(5);
                    combination.ToList().ForEach(c => hand.AddCard(c));
                    return Evaluate(hand);
                }).ToList();

            return evaluations.OrderByDescending(e => e.FullStrength)
                .First();
        }

        public HandValue Evaluate(Hand hand)
        {
            var groupByRank = new short[Deck.Ranks.Count()];
            var groupBySuit = new short[Deck.Suits.Count()];

            var currentCard = Card.Unknown;

            var consecutiveCardsCount = 0;
            var handSize = hand.Size;

            var possibleFiveHighStraight =
                hand.First.Rank == Rank.Two
                && hand.Last.Rank == Rank.Ace;

            HandValue currentValue = null;

            for (var currentCardPosition = 0; currentCardPosition < hand.Size; currentCardPosition++)
            {
                var previousCard = currentCard;
                currentCard = hand[currentCardPosition];

                var currentRank = currentCard.Rank;
                var rankIndex = (short) currentRank - 2;
                var rankOccurence = ++groupByRank[rankIndex];

                var currentSuit = currentCard.Suit;
                var suitIndex = (short) currentSuit - 1;
                var suitOccurence = ++groupBySuit[suitIndex];

                var highestCard = CountConsecutiveCards(
                    previousCard,
                    currentCard,
                    ref consecutiveCardsCount,
                    possibleFiveHighStraight);

                if (consecutiveCardsCount == handSize)
                {
                    currentValue = Straight(hand, highestCard);
                }

                if (suitOccurence == handSize)
                {
                    currentValue = Flush(currentValue, hand, currentSuit);
                }

                if (rankOccurence == 2)
                {
                    currentValue = Pair(currentValue, hand, currentRank, currentCardPosition);
                }
                else if (rankOccurence == 3)
                {
                    currentValue = ThreeOfAKind(currentValue, hand, currentRank, currentCardPosition);
                }
                else if (rankOccurence == 4)
                {
                    currentValue = FourOfAKind(currentValue, hand, currentRank, currentCardPosition, rankOccurence);
                }
            }

            return currentValue ?? new HighCard(hand);
        }

        private static Card CountConsecutiveCards(
            Card previousCard,
            Card currentCard,
            ref int consecutiveCardsCount, bool possibleFiveHighStraight)
        {
            Card highestCard;
            if (previousCard.IsJustBefore(currentCard))
            {
                consecutiveCardsCount++;
                highestCard = currentCard;
            }
            else if (possibleFiveHighStraight
                     && currentCard.Rank == Rank.Ace)
            {
                // Five High Straight A 2 3 4 5
                // We ignore the Ace
                consecutiveCardsCount++;
                highestCard = previousCard;
            }
            else
            {
                consecutiveCardsCount = 0;
                highestCard = Card.Unknown;
            }

            return highestCard;
        }

        private static HandValue Pair(
            HandValue previousValue,
            Hand hand,
            Rank currentRank,
            int cardPosition)
        {
            var newPair = new Pair(hand, currentRank, BuildHighCardForPair(hand, cardPosition));

            switch (previousValue)
            {
                case Pair previousPair:
                    return new TwoPairs(hand,
                        newPair,
                        previousPair,
                        BuildHighCardForTwoPairs(hand, newPair.Rank, previousPair.Rank));
                case ThreeOfAKind previousThreeOfAKind:
                    return new FullHouse(hand, previousThreeOfAKind, newPair);
                default:
                    return newPair;
            }
        }

        private static HighCard BuildHighCardForPair(Hand hand, int cardPosition)
        {
            switch (cardPosition)
            {
                case 1:
                    // CASE 1 : _ _ X Y Z
                    return new HighCard(hand, hand[2], hand[3], hand[4]);
                case 2:
                    // CASE 2 : X _ _ Y Z
                    return new HighCard(hand, hand[0], hand[3], hand[4]);
                case 3:
                    // CASE 3 : X Y _ _ Z
                    return new HighCard(hand, hand[0], hand[1], hand[4]);
                default:
                    // CASE 4 : X Y Z _ _
                    return new HighCard(hand, hand[0], hand[1], hand[2]);
            }
        }

        private static HighCard BuildHighCardForTwoPairs(
            Hand hand,
            Rank rank1,
            Rank rank2)
        {
            // CASE 1 : X _ _ _ _
            if (hand.First.Rank != rank1 &&
                hand.First.Rank != rank2)
            {
                return new HighCard(hand, hand.First);
            }

            // CASE 2 : _ _ X _ _
            if (hand[2].Rank != rank1 &&
                hand[2].Rank != rank2)
            {
                return new HighCard(hand, hand[2]);
            }

            // CASE 3 : _ _ _ _ X
            return new HighCard(hand, hand[4]);
        }

        private static HandValue ThreeOfAKind(
            HandValue previousValue,
            Hand hand,
            Rank rank,
            int cardPosition)
        {
            var newThreeOfAKind =
                new ThreeOfAKind(hand, rank,
                    BuildHighCardForThreeOfAKind(hand, cardPosition));

            switch (previousValue)
            {
                case TwoPairs previousTwoPairs:
                {
                    var pair =
                        previousTwoPairs.HighestPair.Rank == newThreeOfAKind.Rank
                            ? previousTwoPairs.LowestPair
                            : previousTwoPairs.HighestPair;

                    return new FullHouse(hand, newThreeOfAKind, pair);
                }
                default:
                    return newThreeOfAKind;
            }
        }

        private static HighCard BuildHighCardForThreeOfAKind(Hand hand, int cardPosition)
        {
            switch (cardPosition)
            {
                case 2:
                    // CASE 1 : _ _ _ X Y
                    return new HighCard(hand, hand[3], hand[4]);
                case 3:
                    // CASE 2 : X _ _ _ Y
                    return new HighCard(hand, hand[0], hand[4]);
                default:
                    // CASE 3 : X Y _ _ _
                    return new HighCard(hand, hand[0], hand[1]);
            }
        }

        private static HandValue Straight(Hand hand,
            Card highestCard)
        {
            return new Straight(hand, highestCard);
        }

        private static HandValue Flush(
            HandValue previousValue,
            Hand hand,
            Suit currentSuit)
        {
            var flush = new Flush(hand, currentSuit);

            if (previousValue is Straight straight)
            {
                return new StraightFlush(hand, straight, flush);
            }

            return flush;
        }

        private static HandValue FourOfAKind(
            HandValue previousValue,
            Hand hand,
            Rank rank,
            int cardPosition,
            short rankOccurence)
        {
            if (rankOccurence != 4)
            {
                return previousValue;
            }

            Card[] remainingCards;
            switch (cardPosition)
            {
                case 3:
                    // CASE _ _ _ _ X
                    remainingCards = new[] {hand.Last};
                    break;
                default:
                    // CASE X _ _ _ _
                    remainingCards = new[] {hand.First};
                    break;
            }

            return new FourOfAKind(hand, rank,
                new HighCard(hand, remainingCards));
        }
    }
}