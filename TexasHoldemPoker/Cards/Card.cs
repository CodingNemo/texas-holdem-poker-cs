﻿using System;

namespace TexasHoldemPoker.Cards
{
    public class Card : IEquatable<Card>
    {
        public static Card Unknown = Rank.RankLess.Of(Suit.SuitLess);

        private readonly short _rankValue;

        private short _suitValue;

        internal Card(Rank rank, Suit suit)
        {
            Rank = rank;
            _rankValue = (short) rank;
            Suit = suit;
            _suitValue = (short) rank;
            Id = BuildIdFromRankAndSuit(rank, suit);
        }

        public short Id { get; }

        public Rank Rank { get; }

        public Suit Suit { get; }

        public bool Equals(Card other)
        {
            return Id == other.Id;
        }

        public bool IsJustBefore(Card otherCard)
        {
            if (_rankValue == 0) return true;

            if (otherCard._rankValue == 0) return false;

            return _rankValue == otherCard._rankValue - 1;
        }

        public override bool Equals(object obj)
        {
            return Equals((Card) obj);
        }

        public override int GetHashCode()
        {
            return Id;
        }

        public override string ToString()
        {
            return $"{RankToString()}{SuitToString()}";
        }

        private string RankToString()
        {
            switch (Rank)
            {
                case Rank.RankLess:
                    return "X";
                case Rank.Two:
                case Rank.Three:
                case Rank.Four:
                case Rank.Five:
                case Rank.Six:
                case Rank.Seven:
                case Rank.Eight:
                case Rank.Nine:
                case Rank.Ten:
                    return ((int) Rank).ToString();
                case Rank.Jack:
                    return "J";
                case Rank.Queen:
                    return "Q";
                case Rank.King:
                    return "K";
                case Rank.Ace:
                    return "A";
                default:
                    throw new Exception($"Unknown rank {Rank}");
            }
        }

        internal static short BuildIdFromRankAndSuit(Rank rank, Suit suit)
        {
            return (short) ((short) rank * 10 + (short) suit);
        }

        private string SuitToString()
        {
            switch (Suit)
            {
                case Suit.SuitLess:
                    return "X";
                case Suit.Heart:
                    return "♥";
                case Suit.Diamond:
                    return "♦";
                case Suit.Club:
                    return "♣";
                case Suit.Spade:
                    return "♠";
                default:
                    throw new Exception($"Unknown suit {Suit}");
            }
        }
    }
}