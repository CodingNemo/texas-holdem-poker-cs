﻿using System;
using System.Runtime.Serialization;

namespace TexasHoldemPoker.Games.Exceptions
{
    [Serializable]
    public class InvalidCheck : Exception
    {
        public InvalidCheck()
        {
        }

        public InvalidCheck(string message) : base(message)
        {
        }

        public InvalidCheck(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidCheck(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
