﻿using System;
using System.Runtime.Serialization;

namespace TexasHoldemPoker.Games.Exceptions
{
    [Serializable]
    public class InvalidRaise : Exception
    {
        public InvalidRaise()
        {
        }

        public InvalidRaise(string message) : base(message)
        {
        }

        public InvalidRaise(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidRaise(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}