﻿using System;
using System.Runtime.Serialization;

namespace TexasHoldemPoker.Games.Exceptions
{
    [Serializable]
    public class InvalidBet : Exception
    {
        public InvalidBet()
        {
        }

        public InvalidBet(string message) : base(message)
        {
        }

        public InvalidBet(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected InvalidBet(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}