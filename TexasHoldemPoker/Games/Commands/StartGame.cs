﻿using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Games.Domain;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Games.Commands
{
    public class StartGame : New<Game>
    {
        public GameId GameId { get; }
        public Card[] Cards { get; }
        public Blinds Blinds { get; }
        public PlayerId DealerId { get; }
        public InGamePlayer[] Players { get; }

        public object AggregateId => GameId;

        public StartGame(
            GameId gameId, Card[] cards,
            Blinds blinds, PlayerId dealerId,
            InGamePlayer[] players)
        {
            GameId = gameId;
            Cards = cards;
            Blinds = blinds;
            DealerId = dealerId;
            Players = players;
        }
    }
}