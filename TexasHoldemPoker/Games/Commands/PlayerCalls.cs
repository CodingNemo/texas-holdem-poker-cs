﻿using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Games.Commands
{
    public class PlayerCalls : Do<Game>
    {
        public PlayerId PlayerId { get; }

        public object AggregateId { get; }
        public PlayerCalls(GameId gameId, PlayerId playerId)
        {
            AggregateId = gameId;
            PlayerId = playerId;
        }
    }
}