﻿using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Games.Commands
{
    public class PlayerGoesAllIn : Do<Game>
    {
        public object AggregateId { get; }
        public PlayerId PlayerId { get; }

        public PlayerGoesAllIn(GameId gameId, PlayerId playerId)
        {
            AggregateId = gameId;
            PlayerId = playerId;
        }
    }
}