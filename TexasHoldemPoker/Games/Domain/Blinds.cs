﻿using System.Collections.Generic;
using Value;

namespace TexasHoldemPoker.Games.Domain
{
    public class Blinds : ValueType<Blinds>
    {
        public int Small { get; }
        public int Big { get; }

        public Blinds(int small, int big)
        {
            Small = small;
            Big = big;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return new object[] { Small, Big };
        }

        public override string ToString()
        {
            return $"(({Small}/{Big}))";
        }
    }
}