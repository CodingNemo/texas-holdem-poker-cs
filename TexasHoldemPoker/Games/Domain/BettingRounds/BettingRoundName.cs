﻿namespace TexasHoldemPoker.Games.Domain.BettingRounds
{
    public enum BettingRoundName
    {
        None,
        Preflop,
        Flop,
        Turn,
        River,
        Showdown
    }
}