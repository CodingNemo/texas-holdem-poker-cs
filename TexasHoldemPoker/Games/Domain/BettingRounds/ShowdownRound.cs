﻿using System;
using System.Collections.Generic;
using System.Linq;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Hands;
using TexasHoldemPoker.Hands.HandValues;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Games.Domain.BettingRounds
{
    public class ShowdownRound : BettingRound
    {
        private readonly HandEvaluator handEvaluator
            = new HandEvaluator();

        public ShowdownRound(
            GameId gameId, Deck deck,
            Bets bets, Board board, InGameHands hands, Pots pots,
            PlayerId dealerId, IEnumeratePlayerIds players,
            Action<DomainEvent> fire)
            : base(gameId, deck, bets, dealerId, players, fire)
        {
            Board = board;
            Hands = hands;
            Pots = pots;
        }

        public override BettingRoundName NextBettingRoundName => BettingRoundName.None;

        public override BettingRoundName CurrentBettingRoundName => BettingRoundName.Showdown;

        public Board Board { get; }
        public InGameHands Hands { get; }
        public Pots Pots { get; }

        public override BettingRoundStatus Continue()
        {
            StartBettingRound();
            OnStart();
            return BettingRoundStatus.Over;
        }

        protected override void OnStart()
        {
            EvaluateWinnersPerPot();
        }

        private void EvaluateWinnersPerPot()
        {
            foreach (var pot in Pots)
            {
                var winners = Winners(pot).ToList();
                var winnerShare = pot.Value / winners.Count;
                winners.ForEach(w =>
                    Fire(new PlayerWon(GameId, w, winnerShare))
                );
            }
        }

        protected IEnumerable<PlayerId> Winners(Pot pot)
        {
            var handValues = GenerateHandValues(pot);
            HandValue bestHandValue = handValues.Max(t => t.Item2);

            return handValues
                .Where(t => t.Item2 == bestHandValue)
                .Select(t => t.Item1);
        }

        private IEnumerable<Tuple<PlayerId, HandValue>> GenerateHandValues(Pot pot)
        {
            foreach (var playerId in pot.Contributors)
            {
                var player = Players[playerId];

                if (player.LastMove == PlayerMoveType.Fold)
                {
                    continue;
                }

                var hand = Hands[playerId];
                var allCards = Board.Union(hand);
                yield return new Tuple<PlayerId, HandValue>(
                    playerId,
                    handEvaluator.FindBestHand(allCards));
            }
        }
    }
}