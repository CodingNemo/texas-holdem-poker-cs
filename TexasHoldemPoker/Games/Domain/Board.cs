﻿using System.Collections.Generic;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Hands;

namespace TexasHoldemPoker.Games.Domain
{
    public class Board
    {
        private List<Card> communityCards = new List<Card>();

        internal List<Card> Union(Hand hand)
        {
            var allCards = new List<Card>();
            allCards.AddRange(communityCards);
            allCards.Add(hand.First);
            allCards.Add(hand.Last);
            return allCards;
        }

        public void Add(Card card)
        {
            communityCards.Add(card);
        }
    }
}