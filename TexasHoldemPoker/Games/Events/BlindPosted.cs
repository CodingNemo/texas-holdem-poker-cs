﻿using System.Collections.Generic;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;
using Value;
using System.Linq;

namespace TexasHoldemPoker.Games.Events
{
    public class BlindPosted : DomainEvent<BlindPosted>
    {
        public override object AggregateId { get; }

        public PlayerId PlayerId { get; }
        public int BlindValue { get; }

        public BlindPosted(GameId gameId, PlayerId playerId, int blindValue)
        {
            AggregateId = gameId;
            PlayerId = playerId;
            BlindValue = blindValue;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return base.GetAllAttributesToBeUsedForEquality()
                       .Union(new object[] { PlayerId, BlindValue });
        }
    }
}