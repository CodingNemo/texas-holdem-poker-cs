﻿using System.Collections.Generic;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;
using System.Linq;

namespace TexasHoldemPoker.Games.Events
{
    public class PlayerChecked : DomainEvent<PlayerChecked>
    {
        public override object AggregateId { get; }
        public PlayerId PlayerId { get; }

        public PlayerChecked(object gameId, PlayerId playerId)
        {
            AggregateId = gameId;
            PlayerId = playerId;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return base.GetAllAttributesToBeUsedForEquality().Union(new object[] { PlayerId });
        }
    }
}