﻿using System.Collections.Generic;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Framework;
using System.Linq;

namespace TexasHoldemPoker.Games.Events
{
    public class PlayerAllIn : DomainEvent<PlayerAllIn>
    {
        public override object AggregateId { get; }
        public PlayerId PlayerId { get; }

        public PlayerAllIn(object gameId, PlayerId playerId)
        {
            AggregateId = gameId;
            PlayerId = playerId;
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return base.GetAllAttributesToBeUsedForEquality()
                .Union(new object[] { PlayerId });
        }
    }
}