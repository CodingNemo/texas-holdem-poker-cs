﻿using System;
using System.Collections.Generic;
using Value;

namespace TexasHoldemPoker.Players
{
    public class PlayerId : ValueType<PlayerId>
    {
        private readonly string id;

        public static PlayerId New(string id = null)
        {
            return new PlayerId(id ?? Guid.NewGuid().ToString());
        }

        private PlayerId(string id)
        {
            this.id = id;
        }

        public override string ToString()
        {
            return $"{id}";
        }

        protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
        {
            return new[] { id };
        }
    }
}