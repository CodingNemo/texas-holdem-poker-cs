﻿using System;
using NFluent;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Tests.Framework;
using Xunit;

namespace TexasHoldemPoker.Tests.Cards
{
    public class StringToCardParserShould
    {
        public StringToCardParserShould()
        {
            _parser = new StringToCardParser(new Deck());
        }

        private readonly StringToCardParser _parser;

        private void ParseRank(string rankAsString, Rank expectedRank)
        {
            Check.ThatEnum(_parser.Parse(rankAsString + "h").Rank).IsEqualTo(expectedRank);
        }

        private void ParseSuit(string suitAsString, Suit expectedSuit)
        {
            Check.ThatEnum(_parser.Parse("A" + suitAsString).Suit).IsEqualTo(expectedSuit);
        }

        [Fact]
        public void Be_Case_Insensitive_When_Parsing()
        {
            ParseRank("t", Rank.Ten);
            ParseSuit("h", Suit.Heart);
        }

        [Fact]
        public void Fails_When_Parsing_A_Null_Or_Empty_String()
        {
            Check.ThatCode(() => _parser.Parse(null))
                .Throws<Exception>()
                .WithMessage("Provided string is empty. Can't parse anything.");
        }

        [Fact]
        public void Fails_When_Parsing_A_Too_Long_String()
        {
            Check.ThatCode(() => _parser.Parse("Kaq1"))
                .Throws<Exception>()
                .WithMessage("Provided string 'Kaq1' is way too long. It should be 3 chars long max.");
        }

        [Fact]
        public void Fails_When_Parsing_A_Too_Short_String()
        {
            Check.ThatCode(() => _parser.Parse("K"))
                .Throws<Exception>()
                .WithMessage("Provided string 'K' is way too short. It should be 2 chars long min.");
        }

        [Fact]
        public void Fails_When_Parsing_An_Unkown_Rank()
        {
            Check.ThatCode(() => _parser.Parse("1♥"))
                .Throws<Exception>()
                .WithMessage("Unknown rank : 1");
        }


        [Fact]
        public void Fails_When_Parsing_An_Unkown_Suit()
        {
            Check.ThatCode(() => _parser.Parse("Ka"))
                .Throws<Exception>()
                .WithMessage("Unknown suit : a");
        }

        [Fact]
        public void Parse_a_Number_between_2_and_9()
        {
            for (var number = 2; number <= 9; number++) ParseRank(number.ToString(), (Rank) number);
        }

        [Fact]
        public void Parse_the_C_String_as_a_Clubs()
        {
            ParseSuit("C", Suit.Club);
        }

        [Fact]
        public void Parse_the_char_clubs_as_a_Clubs()
        {
            ParseSuit("♣", Suit.Club);
        }

        [Fact]
        public void Parse_the_char_diamond_as_a_Diamond()
        {
            ParseSuit("♦", Suit.Diamond);
        }

        [Fact]
        public void Parse_the_char_heart_as_a_Heart()
        {
            ParseSuit("♥", Suit.Heart);
        }

        [Fact]
        public void Parse_the_char_spade_as_a_Clubs()
        {
            ParseSuit("♠", Suit.Spade);
        }

        [Fact]
        public void Parse_the_D_String_as_a_Diamond()
        {
            ParseSuit("D", Suit.Diamond);
        }

        [Fact]
        public void Parse_the_H_String_as_a_Heart()
        {
            ParseSuit("H", Suit.Heart);
        }

        [Fact]
        public void Parse_the_S_String_as_a_Clubs()
        {
            ParseSuit("S", Suit.Spade);
        }

        [Fact]
        public void Parse_the_string_A_as_an_Ace()
        {
            ParseRank("A", Rank.Ace);
        }

        [Fact]
        public void Parse_the_string_J_as_a_Jack()
        {
            ParseRank("J", Rank.Jack);
        }

        [Fact]
        public void Parse_the_string_K_as_a_King()
        {
            ParseRank("K", Rank.King);
        }

        [Fact]
        public void Parse_the_string_Q_as_a_Queen()
        {
            ParseRank("Q", Rank.Queen);
        }

        [Fact]
        public void Parse_the_string_T_as_a_Ten()
        {
            ParseRank("T", Rank.Ten);
        }
    }
}