﻿using NFluent;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Tests.Framework;
using Xunit;
using static TexasHoldemPoker.Cards.Rank;
using static TexasHoldemPoker.Cards.Suit;

namespace TexasHoldemPoker.Tests.Hands
{
    public class HandShould
    {
        public HandShould()
        {
            _parser = new StringToHandParser(new StringToCardParser(new Deck()));
        }

        private readonly StringToHandParser _parser;

        [Fact]
        public void Be_Equal_To_Another_Hand_With_The_Same_Cards_In_A_Different_Order()
        {
            var hand = _parser.Parse("A♣ K♦ 2♠ J♥ J♦");
            var hand2 = _parser.Parse("K♦ A♣ 2♠ J♦ J♥");

            Check.That(hand).IsEqualTo(hand2);
        }

        [Fact]
        public void Be_Equal_To_Another_Hand_With_The_Same_Cards_In_The_Same_Order()
        {
            var hand = _parser.Parse("A♣ K♦ 2♠ J♥ Q♦");
            var hand2 = _parser.Parse("A♣ K♦ 2♠ J♥ Q♦");

            Check.That(hand).IsEqualTo(hand2);
        }

        [Fact]
        public void Have_The_Same_HashCode_When_Two_Hands_Are_Equal()
        {
            var hand = _parser.Parse("A♣ K♦ 2♠ J♥ J♦");
            var hand2 = _parser.Parse("K♦ A♣ 2♠ J♦ J♥");

            Check.That(hand.GetHashCode()).IsEqualTo(hand2.GetHashCode());
        }


        [Fact]
        public void Not_Be_Equal_To_Another_Hand_With_Differents_Cards()
        {
            var hand = _parser.Parse("A♣ K♦ 2♠ J♥ J♦");
            var hand2 = _parser.Parse("5♦ 2♣ T♣ Q♦ 9♦");

            Check.That(hand).Not.IsEqualTo(hand2);
        }

        [Fact]
        public void Returns_All_Added_Cards_In_The_Ascending_Order_Of_Rank_And_Suit()
        {
            var hand = _parser.Parse("K♦ 2♠ A♣ J♥");

            Check.That(hand.Size).IsEqualTo(4);
            Check.That(hand[0]).Is(Two.Of(Spade));
            Check.That(hand[1]).Is(Jack.Of(Heart));
            Check.That(hand[2]).Is(King.Of(Diamond));
            Check.That(hand[3]).Is(Ace.Of(Club));
        }
    }
}