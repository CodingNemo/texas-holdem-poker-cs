﻿using NFluent;
using TexasHoldemPoker.Hands.HandValues;
using TexasHoldemPoker.Tests.Framework;
using Xunit;
using static TexasHoldemPoker.Tests.Hands.HandValues.HandValueFactory;

namespace TexasHoldemPoker.Tests.Hands.HandValues
{
    public class PairShould
    {
        [Fact]
        public void Beat_Another_Pair_With_A_Lesser_Rank()
        {
            Check.That(This<Pair>("A♥ A♦ K♠ J♠ 4♣"))
                .Beats(This<Pair>("4♥ 4♦ T♠ Q♠ 3♣"));
        }

        [Fact]
        public void Beat_Another_Pair_With_The_Same_Rank_But_With_A_Smaller_Kicker()
        {
            Check.That(This<Pair>("A♥ A♦ K♠ J♠ 4♣"))
                .Beats(This<Pair>("A♥ A♦ K♠ J♠ 3♣"));
        }

        [Fact]
        public void Beats_The_Biggest_HighCard()
        {
            Check.That(This<Pair>("2♦ 2♣ 3♥ 4♣ 5♣"))
                .Beats(This<HighCard>("9♥ J♥ Q♥ K♥ A♦"));
        }

        [Fact]
        public void Not_Beat_Another_Pair_With_The_Same_Strength()
        {
            Check.That(This<Pair>("A♥ A♦ K♠ J♠ 4♣"))
                .Not.Beats(This<Pair>("A♠ A♥ K♦ J♠ 4♣"));
        }
    }
}