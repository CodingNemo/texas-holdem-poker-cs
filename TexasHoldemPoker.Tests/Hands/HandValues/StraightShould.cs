﻿using NFluent;
using TexasHoldemPoker.Hands.HandValues;
using TexasHoldemPoker.Tests.Framework;
using Xunit;
using static TexasHoldemPoker.Tests.Hands.HandValues.HandValueFactory;

namespace TexasHoldemPoker.Tests.Hands.HandValues
{
    public class StraightShould
    {
        [Fact]
        public void Beat_Another_Straight_With_A_Smaller_HighCard()
        {
            Check.That(This<Straight>("2♠ 3♦ 4♦ 5♥ 6♣"))
                .Beats(This<Straight>("A♠ 2♦ 3♦ 4♥ 5♣"));
        }

        [Fact]
        public void Beat_The_Biggest_HighCard()
        {
            Check.That(This<Straight>("A♠ 2♦ 3♦ 4♥ 5♣"))
                .Beats(This<HighCard>("A♥ K♦ Q♥ J♦ 9♣"));
        }

        [Fact]
        public void Beat_The_Biggest_Pair()
        {
            Check.That(This<Straight>("A♠ 2♦ 3♦ 4♥ 5♣"))
                .Beats(This<Pair>("A♥ A♦ K♥ J♦ Q♣"));
        }

        [Fact]
        public void Beat_The_Biggest_ThreeOfAKind()
        {
            Check.That(This<Straight>("A♠ 2♦ 3♦ 4♥ 5♣"))
                .Beats(This<ThreeOfAKind>("A♥ A♦ A♣ K♦ Q♣"));
        }


        [Fact]
        public void Beat_The_Biggest_TwoPairs()
        {
            Check.That(This<Straight>("A♠ 2♦ 3♦ 4♥ 5♣"))
                .Beats(This<TwoPairs>("A♥ A♦ K♥ K♦ Q♣"));
        }

        [Fact]
        public void Not_Beat_Another_Straight_With_The_Same_Strength()
        {
            Check.That(This<Straight>("A♠ 2♦ 3♦ 4♥ 5♣"))
                .Not.Beats(This<Straight>("A♦ 2♣ 3♣ 4♣ 5♥"));
        }
    }
}