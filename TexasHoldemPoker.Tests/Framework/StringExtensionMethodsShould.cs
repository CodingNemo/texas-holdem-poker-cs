﻿using NFluent;
using TexasHoldemPoker.Framework;
using Xunit;

namespace TexasHoldemPoker.Tests.Framework
{
    public class StringExtensionMethodsShould
    {
        [Fact]
        public void Split_A_String_On_Each_UpperCase_char()
        {
            Check.That("IAmGroot".SplitOnUpperCaseLetters())
                .ContainsExactly("I", "am", "groot");
        }


        [Fact]
        public void Capitalize_a_word_by_changing_the_first_char_case_to_Upper()
        {
            Check.That("benoit".Capitalize()).IsEqualTo("Benoit");
        }
    }
}
