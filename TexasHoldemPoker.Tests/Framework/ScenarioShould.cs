﻿using NFluent;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using TexasHoldemPoker.Framework;
using TexasHoldemPoker.Tests.Framework.ScenarioGeneration;
using Value;
using Xunit;

namespace TexasHoldemPoker.Tests.Framework
{
    public class ScenarioShould
    {
        class Something : Aggregate
        {
            public Something(History history, ILogEvents log, StateProjection state) : base(history, log, state)
            {
            }
        }

        class SomethingId : ValueType<SomethingId>
        {
            public SomethingId(int value)
            {
                Value = value;
            }

            public int Value { get; }

            protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
            {
                return new object[] { Value };
            }
        }

        class SomeoneDidAThing : DomainEvent<SomeoneDidAThing>
        {
            public override object AggregateId { get; }

            public SomeoneDidAThing(SomethingId id)
            {
                AggregateId = id;
            }
        }

        class SomethingElseHappened : DomainEvent<SomethingElseHappened>
        {
            public override object AggregateId { get; }

            public SomethingElseHappened(SomethingId id)
            {
                AggregateId = id;
            }
        }

        class SomeoneDoSomething : Do<Something>
        {
            public object AggregateId { get; }

            public SomeoneDoSomething(SomethingId id)
            {
                AggregateId = id;
            }
        }


        [Fact]
        public void Export_A_Given_When_Then_Scenario_As_Readable()
        {
            var id = new SomethingId(13);

            var scenarioText = WriteScenario(scenario =>
                scenario.Given(Step.FromType<Something>())
                        .WithHistory(new[] { Step.FromInstance(new SomeoneDidAThing(id)) })
                        .When(Step.FromInstance(new SomeoneDoSomething(id)))
                        .Then(
                            Step.FromInstance(new SomethingElseHappened(id)),
                            Step.FromInstance(new SomethingElseHappened(id))));

            Check.That(scenarioText).IsEqualTo(
               "Scenario: Scenario should export a given when then scenario as readable" + Environment.NewLine +
               "   Given a Something" + Environment.NewLine +
               "     And Someone did a thing" + Environment.NewLine +
               "   When Someone do something" + Environment.NewLine +
               "   Then Something else happened" + Environment.NewLine +
               "    And Something else happened" + Environment.NewLine);
        }

        private static string WriteScenario(Action<Scenario> play)
        {
            var builder = new StringBuilder();

            var exportToConsole = new TextScenarioExporter(line => builder.AppendLine(line));

            var scenario = new Scenario(
                    new StackTrace()
                        .FindCallingTestMethod()
                        .NameAsReadableSentence(),
                new[] { exportToConsole });

            play(scenario);

            scenario.Export();

            return builder.ToString();
        }
    }
}
