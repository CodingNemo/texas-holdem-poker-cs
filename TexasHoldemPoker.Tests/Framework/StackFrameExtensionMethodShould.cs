﻿using NFluent;
using System;
using System.Diagnostics;
using System.Reflection;
using Xunit;

namespace TexasHoldemPoker.Tests.Framework
{
    public class StackFrameExtensionMethodShould
    {
        [Fact]
        public void Find_The_Calling_Test_Method()
        {
            var callingTestMethod = new StackTrace().FindCallingTestMethod();
            Check.That(callingTestMethod).IsNotNull();

            Check.That(callingTestMethod.Name).IsEqualTo(nameof(Find_The_Calling_Test_Method));
            Check.That(callingTestMethod.ReflectedType).IsEqualTo(typeof(StackFrameExtensionMethodShould));
        }


        [Fact]
        public void Find_The_Calling_Test_Method_When_Many_Intermediate_Calls()
        {
            var callingTestMethod = new StackTrace().FindCallingTestMethod();
            Check.That(callingTestMethod).IsNotNull();

            Check.That(callingTestMethod.Name).IsEqualTo(nameof(Find_The_Calling_Test_Method_When_Many_Intermediate_Calls));
            Check.That(callingTestMethod.ReflectedType).IsEqualTo(typeof(StackFrameExtensionMethodShould));
        }


        private static MethodBase IntermediateCall()
        {
            var intermediateLambda = new Func<MethodBase>(() => { return AnotherIntermediateCall(); });
            return intermediateLambda();
        }

        private static MethodBase AnotherIntermediateCall()
        {
            return new StackTrace().FindCallingTestMethod();
        }
    }
}
