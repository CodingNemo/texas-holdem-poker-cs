﻿using System;
using System.Collections.Generic;
using NFluent;
using TexasHoldemPoker.Framework;
using Value;
using Xunit;

namespace TexasHoldemPoker.Tests.Framework
{
    public class DispatcherShould
    {
        public class SomeId : ValueType<SomeId>
        {
            private readonly string id;

            public SomeId(string id)
            {
                this.id = id;
            }

            protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
            {
                return new[] { id };
            }

            public override string ToString()
            {
                return id;
            }
        }

        public class SomeOtherCommand : Do<SomeAggregate>
        {
            public SomeOtherCommand(SomeId id)
            {
                SomeId = id;
            }

            public object AggregateId => SomeId;

            public SomeId SomeId { get; }
        }

        public class SomeCreationCommand : New<SomeAggregate>
        {
            public SomeCreationCommand(SomeId id)
            {
                SomeId = id;
            }

            public object AggregateId => SomeId;

            public SomeId SomeId { get; }
        }

        public class SomeCreated : DomainEvent<SomeCreated>
        {
            public override object AggregateId { get; }
            public SomeCreated(SomeId id)
            {
                AggregateId = id;
            }

            protected override IEnumerable<object> GetAllAttributesToBeUsedForEquality()
            {
                return new[] { AggregateId };
            }
        }

        public class SomeCommand : Do<SomeAggregate>
        {
            public SomeCommand(SomeId id)
            {
                SomeId = id;
            }

            public object AggregateId => SomeId;

            public SomeId SomeId { get; }
        }

        public class SomeEvent : DomainEvent<SomeEvent>
        {

            public SomeId SomeId { get; }

            public override object AggregateId => SomeId;

            public SomeEvent(SomeId id)
            {
                SomeId = id;
            }
        }

        public class SomeAggregate :
            Aggregate,
            IHandle<SomeCommand>,
            IHandle<SomeCreationCommand>
        {
            private class SomeState : StateProjection { }

            public SomeAggregate(
                History history, ILogEvents log) : base(history, log, new SomeState())
            {
            }

            public void Handle(SomeCommand command)
            {
                Fire(new SomeEvent(command.SomeId));
            }

            public void Handle(SomeCreationCommand command)
            {
                Fire(new SomeCreated(command.SomeId));
            }
        }

        public class OneElementStore : Storage
        {
            private readonly object element;

            public OneElementStore(object element)
            {
                this.element = element;
            }

            public T Create<T>() where T : Aggregate
            {
                throw new NotImplementedException();
            }

            public SearchResult<T> SearchById<T>(object id)
                where T : Aggregate
            {
                return SearchResult<T>.Found((T)element);
            }
        }

        public class EmptyStore : Storage
        {
            private readonly ILogEvents log;

            public EmptyStore(ILogEvents log)
            {
                this.log = log;
            }

            public T Create<T>()
                where T : Aggregate
            {
                return Aggregate.Create<T>(log);
            }

            public SearchResult<T> SearchById<T>(object id)
                where T : Aggregate
            {
                return SearchResult<T>.NotFound;
            }
        }

        [Fact]
        public void Give_A_Command_To_The_Appropriate_Aggregate()
        {
            var id = new SomeId("plop");
            var command = new SomeCommand(id);

            var log = new InMemoryEventStore();

            var aggregate = new SomeAggregate(History.Clean, log);

            var store = new OneElementStore(aggregate);

            new Dispatcher(store)
                .Dispatch(command);

            Check.That(log.HistoryOf(id)).ContainsExactly(new SomeEvent(id));
        }

        [Fact]
        public void Fail_When_The_Command_Can_Not_Be_Handled_By_The_Aggregate()
        {
            var id = new SomeId("plop");
            var command = new SomeOtherCommand(id);

            var log = new InMemoryEventStore();

            var aggregate = new SomeAggregate(History.Clean, log);

            var store = new OneElementStore(aggregate);

            Check.ThatCode(() => new Dispatcher(store).Dispatch(command))
                .Throws<FailToDispatchCommand>()
                .WithMessage("Aggregate plop of type SomeAggregate can not handle a command of type SomeOtherCommand.");
        }

        [Fact]
        public void Fail_When_The_The_Aggregate_Does_Not_Exist()
        {
            var id = new SomeId("plop");
            var command = new SomeCommand(id);

            var log = new InMemoryEventStore();

            var store = new EmptyStore(log);

            Check.ThatCode(() => new Dispatcher(store).Dispatch(command))
                .Throws<FailToDispatchCommand>()
                .WithMessage("Could not find any aggregate with id plop.");
        }

        [Fact]
        public void Create_A_New_Aggregate_When_Command_Is_Creation()
        {
            var log = new InMemoryEventStore();

            var id = new SomeId("plop");
            var command = new SomeCreationCommand(id);

            var store = new EmptyStore(log);

            new Dispatcher(store)
                .Dispatch(command);

            Check.That(log.HistoryOf(id))
                 .ContainsExactly(new SomeCreated(id));
        }
    }
}
