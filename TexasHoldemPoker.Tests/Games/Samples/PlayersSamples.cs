﻿using System.Collections.Generic;
using System.Linq;
using TexasHoldemPoker.Games.Domain;
using TexasHoldemPoker.Players;

using static TexasHoldemPoker.Tests.Framework.ArrayFactories;

namespace TexasHoldemPoker.Tests.Games.Samples
{
    public class PlayersSamples
    {
        public readonly InGamePlayer SmallBlind = Player("SB ");
        public PlayerId SmallBlindId => SmallBlind.Id;

        public readonly InGamePlayer BigBlind = Player("BB ");
        public PlayerId BigBlindId => BigBlind.Id;

        public readonly InGamePlayer UnderTheGun = Player("UTG");
        public PlayerId UnderTheGunId => UnderTheGun.Id;


        public readonly InGamePlayer Dealer = Player("D  ");
        public PlayerId DealerId => Dealer.Id;

        public InGamePlayer[] All => Many(SmallBlind, BigBlind, UnderTheGun, Dealer);
        public Dictionary<PlayerId, int> AllAsDict => All.ToDictionary(p => p.Id, p => p.ChipsValue);
        public IEnumeratePlayerIds AllIds => new InGamePlayers(All);

        private static InGamePlayer Player(string name, int chips = 30_000) => new InGamePlayer(PlayerId.New(name), chips);
    }
}
