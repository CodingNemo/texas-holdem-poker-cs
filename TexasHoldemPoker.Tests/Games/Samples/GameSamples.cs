﻿using System;
using System.Collections.Generic;
using System.Linq;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Games.Domain;
using TexasHoldemPoker.Games.Domain.BettingRounds;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Tests.Framework;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Tests.Games.Samples
{
    public class GameSamples
    {
        public readonly PlayersSamples Players = new PlayersSamples();
        public readonly HandsSamples Hands = new HandsSamples();
        public readonly CardsSamples Cards = new CardsSamples();

        public class PreflopSamples
        {
            private readonly GameSamples game;

            public PreflopSamples(GameSamples game)
            {
                this.game = game;
            }

            public History Started()
            {
                return new History(
                    new BettingRoundStarted(game.Id, BettingRoundName.Preflop, game.Players.BigBlindId),
                    new CardDealtToPlayer(game.Id, game.Players.SmallBlindId, game.Hands.SmallBlind.First),
                    new CardDealtToPlayer(game.Id, game.Players.BigBlindId, game.Hands.BigBlind.First),
                    new CardDealtToPlayer(game.Id, game.Players.UnderTheGunId, game.Hands.UnderTheGun.First),
                    new CardDealtToPlayer(game.Id, game.Players.DealerId, game.Hands.Dealer.First),
                    new CardDealtToPlayer(game.Id, game.Players.SmallBlindId, game.Hands.SmallBlind.Last),
                    new CardDealtToPlayer(game.Id, game.Players.BigBlindId, game.Hands.BigBlind.Last),
                    new CardDealtToPlayer(game.Id, game.Players.UnderTheGunId, game.Hands.UnderTheGun.Last),
                    new CardDealtToPlayer(game.Id, game.Players.DealerId, game.Hands.Dealer.Last),
                    new BlindPosted(game.Id, game.Players.SmallBlindId, game.Blinds.Small),
                    new BlindPosted(game.Id, game.Players.BigBlindId, game.Blinds.Big),
                    new PlayerTurnStarted(game.Id, game.Players.UnderTheGunId)
                );
            }

            public History UTGCalled()
            {
                return new History(
                    new PlayerCalled(game.Id, game.Players.UnderTheGunId),
                    new PlayerTurnStarted(game.Id, game.Players.DealerId));
            }

            public History DealerFolded()
            {
                return new History(
                        new PlayerFolded(game.Id, game.Players.DealerId),
                        new PlayerTurnStarted(game.Id, game.Players.SmallBlindId)
                    );
            }

            public History SBCalled()
            {
                return new History(
                    new PlayerCalled(game.Id, game.Players.SmallBlindId),
                    new PlayerTurnStarted(game.Id, game.Players.BigBlindId)
                    );
            }

            public History BBChecked()
            {
                return new History(
                    new PlayerChecked(game.Id, game.Players.BigBlindId)
                );
            }

            private History IsOver()
            {
                return new History(
                    new BettingRoundIsOver(game.Id, BettingRoundName.Preflop,
                                    new Dictionary<PlayerId, int>()
                                    {
                                        [game.Players.UnderTheGunId] = game.Blinds.Big,
                                        [game.Players.SmallBlindId] = game.Blinds.Big,
                                        [game.Players.BigBlindId] = game.Blinds.Big,
                                    }));
            }

            public History Happened()
            {
                return Started()
                        .Then(UTGCalled)
                        .Then(DealerFolded)
                        .Then(SBCalled)
                        .Then(BBChecked)
                        .Then(IsOver);
            }
        }

        public PreflopSamples Preflop => new PreflopSamples(this);

        public class FlopSamples
        {
            private readonly GameSamples game;

            public FlopSamples(GameSamples game)
            {
                this.game = game;
            }

            public History Started()
            {
                return new History(
                    new BettingRoundStarted(game.Id, BettingRoundName.Flop, game.Players.DealerId),
                    new CardBurnt(game.Id, game.Cards.BurntBeforeFlop),
                    new FlopDealt(game.Id, game.Cards.Flop),
                    new PlayerTurnStarted(game.Id, game.Players.SmallBlindId)
                    );
            }

            public History SBBet()
            {
                return new History(
                    new PlayerBet(game.Id, game.Players.SmallBlindId, game.Blinds.Big * 2),
                    new PlayerTurnStarted(game.Id, game.Players.BigBlindId)
                    );
            }

            public History BBFolded()
            {
                return new History(
                    new PlayerFolded(game.Id, game.Players.BigBlindId),
                    new PlayerTurnStarted(game.Id, game.Players.UnderTheGunId)
                    );
            }

            public History UTGRaised()
            {
                return new History(
                    new PlayerRaised(game.Id, game.Players.UnderTheGunId, game.Blinds.Big * 4),
                    new PlayerTurnStarted(game.Id, game.Players.SmallBlindId)
                    );
            }

            public History SBCalled()
            {
                return new History(
                    new PlayerCalled(game.Id, game.Players.SmallBlindId));
            }

            private History IsOver()
            {
                return new History(
                        new BettingRoundIsOver(game.Id, BettingRoundName.Flop,
                                    new Dictionary<PlayerId, int>()
                                    {
                                        [game.Players.UnderTheGunId] = game.Blinds.Big * 2 + game.Blinds.Big * 4,
                                        [game.Players.SmallBlindId] = game.Blinds.Big * 2 + game.Blinds.Big * 4,
                                    })
                    );
            }

            public History Happened()
            {
                return Started()
                    .Then(SBBet)
                    .Then(BBFolded)
                    .Then(UTGRaised)
                    .Then(SBCalled)
                    .Then(IsOver);
            }

        }

        public FlopSamples Flop => new FlopSamples(this);

        public class TurnSamples
        {
            private readonly GameSamples game;

            public TurnSamples(GameSamples game)
            {
                this.game = game;
            }

            public History Started()
            {
                return new History(
                    new BettingRoundStarted(game.Id, BettingRoundName.Turn, game.Players.DealerId),
                    new CardBurnt(game.Id, game.Cards.BurntBeforeTurn),
                    new TurnDealt(game.Id, game.Cards.Turn),
                    new PlayerTurnStarted(game.Id, game.Players.SmallBlindId)
                    );
            }

            public History SBChecked()
            {
                return new History(
                    new PlayerChecked(game.Id, game.Players.SmallBlindId),
                            new PlayerTurnStarted(game.Id, game.Players.UnderTheGunId));
            }

            public History UTGChecked()
            {
                return new History(
                    new PlayerChecked(game.Id, game.Players.UnderTheGunId));
            }

            private History IsOver()
            {
                return new History(
                        new BettingRoundIsOver(game.Id, BettingRoundName.Turn, new Dictionary<PlayerId, int>())
                    );
            }

            public History Happened()
            {
                return Started()
                     .Then(SBChecked)
                     .Then(UTGChecked)
                     .Then(IsOver);
            }

        }

        public TurnSamples Turn => new TurnSamples(this);

        public class RiverSamples
        {
            private GameSamples game;

            public RiverSamples(GameSamples game)
            {
                this.game = game;
            }

            public History Started()
            {
                return new History(
                    new BettingRoundStarted(game.Id, BettingRoundName.River, game.Players.DealerId),
                    new CardBurnt(game.Id, game.Cards.BurntBeforeRiver),
                    new RiverDealt(game.Id, game.Cards.River),
                    new PlayerTurnStarted(game.Id, game.Players.SmallBlindId));
            }

            public History SBWentAllIn()
            {
                return new History(
                    new PlayerAllIn(game.Id, game.Players.SmallBlindId),
                    new PlayerTurnStarted(game.Id, game.Players.UnderTheGunId)
                    );
            }

            public History UTGCalled()
            {
                return new History(
                        new PlayerCalled(game.Id, game.Players.UnderTheGunId)
                    );
            }

            private History IsOver()
            {
                return new History(
                    new BettingRoundIsOver(
                            game.Id, BettingRoundName.River,
                            new Dictionary<PlayerId, int>()
                            {
                                [game.Players.SmallBlindId] = 25000,
                                [game.Players.UnderTheGunId] = 25000,
                            })
                    );
            }

            public History Happened()
            {
                return Started()
                    .Then(SBWentAllIn)
                    .Then(UTGCalled)
                    .Then(IsOver);
            }
        }

        public RiverSamples River => new RiverSamples(this);

        public History Started()
        {
            return new History(new GameIsStarted(Id, DeckCards, Blinds, Players.DealerId, Players.AllAsDict));
        }

        public Card[] DeckCards => CheckDeck(Hands.AllCards
                        .Plus(Cards.BurntBeforeFlop)
                        .Plus(Cards.Flop)
                        .Plus(Cards.BurntBeforeTurn)
                        .Plus(Cards.Turn)
                        .Plus(Cards.BurntBeforeRiver)
                        .Plus(Cards.River));

        private static Card[] CheckDeck(Card[] cards)
        {
            var cardOccurences =
                                cards.GroupBy(c => c)
                                 .ToDictionary(c => c, c => c.Count())
                                 .Where(kvp => kvp.Value > 1);

            if (cardOccurences.Any())
            {
                var displayDupplicated =
                    string.Join(",", cardOccurences.Select(co => $"{co.Key}:{co.Value}"));

                throw new Exception(
                    $"INVALID DECK. DUPPLICATED CARDS [{displayDupplicated}]");

            }

            return cards;
        }

        public readonly Blinds Blinds = new Blinds(500, 1_000);

        public readonly GameId Id = GameId.New(TableId.New("TABLE"), 0);
    }
}
