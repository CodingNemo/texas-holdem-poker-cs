﻿using System.Collections.Generic;
using TexasHoldemPoker.Games;
using TexasHoldemPoker.Games.Commands;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tests.Framework;
using TexasHoldemPoker.Tests.Games.Samples;
using Xunit;
using Xunit.Abstractions;
using static TexasHoldemPoker.Games.Domain.BettingRounds.BettingRoundName;

namespace TexasHoldemPoker.Tests.Games.Scenarios
{
    public class RiverShould : Feature
    {
        public readonly GameSamples game = new GameSamples();

        public RiverShould(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        [Fact]
        public void Start_When_Turn_BettingRound_Is_Over() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Happened)
                .And(game.Turn.Started)
                .And(game.Turn.SBChecked)
                .When(new PlayerChecks(game.Id, game.Players.UnderTheGunId))
                .Then(
                    CheckNewHistory.Skip(2)
                    .Equals(
                        new BettingRoundStarted(game.Id, River, game.Players.DealerId)));

        [Fact]
        public void Deal_River_Card_When_Starting() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Happened)
                .And(game.Turn.Started)
                .And(game.Turn.SBChecked)
                .When(new PlayerChecks(game.Id, game.Players.UnderTheGunId))
                .Then(
                    CheckNewHistory.Skip(3)
                    .Equals(
                        new CardBurnt(game.Id, game.Cards.BurntBeforeRiver),
                        new RiverDealt(game.Id, game.Cards.River)));

        [Fact]
        public void Start_SmallBlind_Player_Turn_When_Started() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Happened)
                .And(game.Turn.Started)
                .And(game.Turn.SBChecked)
                .When(new PlayerChecks(game.Id, game.Players.UnderTheGunId))
                .Then(
                    CheckNewHistory.Skip(5)
                    .Equals(
                        new PlayerTurnStarted(game.Id, game.Players.SmallBlindId)));

        [Fact]
        public void Fire_PlayerAllIn_Event_When_SmallBlind_Goes_AllIn() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Happened)
                .And(game.Turn.Happened)
                .And(game.River.Started)
                .When(new PlayerGoesAllIn(game.Id, game.Players.SmallBlindId))
                .Then(
                    CheckNewHistory.StartsWith(
                        new PlayerAllIn(game.Id, game.Players.SmallBlindId),
                        new PlayerTurnStarted(game.Id, game.Players.UnderTheGunId)));

        [Fact]
        public void Fire_PlayerCalled_Event_When_UTG_Player_Calls() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Happened)
                .And(game.Turn.Happened)
                .And(game.River.Started)
                .And(game.River.SBWentAllIn)
                .When(new PlayerCalls(game.Id, game.Players.UnderTheGunId))
                .Then(
                    CheckNewHistory.StartsWith(
                        new PlayerCalled(game.Id, game.Players.UnderTheGunId)));

        [Fact]
        public void End_When_All_Players_Have_Finished() =>
            Given.A<Game>()
                .WithHistory(game.Started)
                .And(game.Preflop.Happened)
                .And(game.Flop.Happened)
                .And(game.Turn.Happened)
                .And(game.River.Started)
                .And(game.River.SBWentAllIn)
                .When(new PlayerCalls(game.Id, game.Players.UnderTheGunId))
                .Then(
                    CheckNewHistory.Skip(1)
                                .Equals(
                                    new BettingRoundIsOver(
                                        game.Id, River,
                                        new Dictionary<PlayerId, int>()
                                        {
                                            [game.Players.SmallBlindId] = 23000,
                                            [game.Players.UnderTheGunId] = 23000,
                                        })));
    }
}