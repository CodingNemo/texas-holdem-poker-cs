﻿using System.Linq;
using TexasHoldemPoker.Cards;
using TexasHoldemPoker.Framework;
using TexasHoldemPoker.Games;
using TexasHoldemPoker.Games.Commands;
using TexasHoldemPoker.Games.Domain;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Hands;
using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;
using TexasHoldemPoker.Tests.Framework;
using Xunit;
using Xunit.Abstractions;

using static TexasHoldemPoker.Tests.Framework.ArrayFactories;
using static TexasHoldemPoker.Games.Domain.BettingRounds.BettingRoundName;
using static TexasHoldemPoker.Cards.Rank;
using static TexasHoldemPoker.Cards.Suit;

using static TexasHoldemPoker.Tests.Games.Samples.HandsSamples;
using System.Collections.Generic;

namespace TexasHoldemPoker.Tests.Games.Scenarios
{
    public class AllInPreflopShould : Feature
    {
        public AllInPreflopShould(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        public class HeadsUpGameSample
        {
            public GameId Id = GameId.New(TableId.New());

            public readonly Card BurntBeforeFlop = Ace.Of(Spade);
            public readonly Card[] Flop = { Three.Of(Club), Queen.Of(Heart), Four.Of(Diamond) };

            public readonly Card BurntBeforeTurn = Nine.Of(Heart);
            public readonly Card Turn = King.Of(Diamond);

            public readonly Card BurntBeforeRiver = Queen.Of(Diamond);
            public readonly Card River = Jack.Of(Spade);

            public Card[] DeckCards => FromHands(DealerPlayerHand, OtherPlayerHand)
                                    .Plus(BurntBeforeFlop)
                                    .Plus(Flop)
                                    .Plus(BurntBeforeTurn)
                                    .Plus(Turn)
                                    .Plus(BurntBeforeRiver)
                                    .Plus(River);

            public Blinds Blinds = new Blinds(1000, 2000);

            public PlayerId DealerId = PlayerId.New("[ONE]");
            public InGamePlayer Dealer => new InGamePlayer(DealerId, 6000);


            public PlayerId OtherPlayerId = PlayerId.New("[TWO]");
            public InGamePlayer OtherPlayer => new InGamePlayer(OtherPlayerId, 9000);


            public InGamePlayer[] Players => Many(Dealer, OtherPlayer);

            public Hand OtherPlayerHand = new Hand(2)
                                                .AddCard(Two.Of(Club))
                                                .AddCard(Two.Of(Heart));

            public Hand DealerPlayerHand = new Hand(2)
                                                .AddCard(Jack.Of(Diamond))
                                                .AddCard(Ace.Of(Diamond));

            public History Started()
            {
                return new History(new GameIsStarted(Id, DeckCards, Blinds, DealerId, Players.ToDictionary(p => p.Id, p => p.ChipsValue)));
            }

            public History PreflopStarted()
            {
                return new History(
                    new BettingRoundStarted(Id, Preflop, OtherPlayerId),                   
                    new CardDealtToPlayer(Id, DealerId, DealerPlayerHand.First),
                    new CardDealtToPlayer(Id, OtherPlayerId, OtherPlayerHand.First),
                    new CardDealtToPlayer(Id, DealerId, DealerPlayerHand.Last),
                    new CardDealtToPlayer(Id, OtherPlayerId, OtherPlayerHand.Last),
                    new BlindPosted(Id, DealerId, Blinds.Small),
                    new BlindPosted(Id, OtherPlayerId, Blinds.Big),
                    new PlayerTurnStarted(Id, DealerId)
                    );

            }
        }

        public readonly HeadsUpGameSample game = new HeadsUpGameSample();

        [Fact]
        public void Run_All_Betting_Round_When_Only_One_Player_Is_In_Game_And_All_The_Others_Are_All_In() =>
         Given.A<Game>()
              .WithHistory(game.Started)
              .And(game.PreflopStarted)
              .And(new PlayerTurnStarted(game.Id, game.DealerId))
              .And(new PlayerAllIn(game.Id, game.DealerId))
              .And(new PlayerTurnStarted(game.Id, game.OtherPlayerId))
              .When(new PlayerCalls(game.Id, game.OtherPlayerId))
              .Then(
                    CheckNewHistory.Is(
                        new PlayerCalled(game.Id, game.OtherPlayerId),
                        new BettingRoundIsOver(game.Id, Preflop, new Dictionary<PlayerId, int>
                        {
                            [game.DealerId] = game.Dealer.ChipsValue,
                            [game.OtherPlayerId] = game.Dealer.ChipsValue
                        }),
                        new BettingRoundStarted(game.Id, Flop, game.DealerId),
                        new CardBurnt(game.Id, game.BurntBeforeFlop),
                        new FlopDealt(game.Id, game.Flop),
                        new BettingRoundIsOver(game.Id, Flop, new Dictionary<PlayerId, int>()),
                        new BettingRoundStarted(game.Id, Turn, game.DealerId),
                        new CardBurnt(game.Id, game.BurntBeforeTurn),
                        new TurnDealt(game.Id, game.Turn),
                        new BettingRoundIsOver(game.Id, Turn, new Dictionary<PlayerId, int>()),
                        new BettingRoundStarted(game.Id, River, game.DealerId),
                        new CardBurnt(game.Id, game.BurntBeforeRiver),
                        new RiverDealt(game.Id, game.River),
                        new BettingRoundIsOver(game.Id, River, new Dictionary<PlayerId, int>()),
                        new BettingRoundStarted(game.Id, Showdown, game.DealerId),
                        new PlayerWon(game.Id, game.DealerId, game.Dealer.ChipsValue * 2)
                        ));
    }
}
