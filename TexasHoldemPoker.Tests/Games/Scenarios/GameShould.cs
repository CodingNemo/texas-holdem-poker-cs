﻿using TexasHoldemPoker.Games;
using TexasHoldemPoker.Games.Commands;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Tests.Framework;
using TexasHoldemPoker.Tests.Games.Samples;
using Xunit;
using Xunit.Abstractions;

namespace TexasHoldemPoker.Tests.Games.Scenarios
{
    public class GameShould : Feature
    {
        public readonly GameSamples game = new GameSamples();

        public GameShould(ITestOutputHelper outputHelper)
            : base(outputHelper)
        {
        }

        [Fact]
        public void Fire_A_GameStarted_Event_When_A_Game_Has_Been_Started() =>
            Given.A<Game>()
                .When(new StartGame(game.Id, game.DeckCards, game.Blinds, game.Players.DealerId, game.Players.All))
                .Then(
                    CheckNewHistory.StartsWith(
                        new GameIsStarted(game.Id, game.DeckCards, game.Blinds, game.Players.DealerId, game.Players.AllAsDict))
                );
    }
}