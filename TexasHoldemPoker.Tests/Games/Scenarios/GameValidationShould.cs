﻿using TexasHoldemPoker.Games;
using TexasHoldemPoker.Games.Commands;
using TexasHoldemPoker.Games.Events;
using TexasHoldemPoker.Games.Exceptions;
using TexasHoldemPoker.Tests.Framework;
using TexasHoldemPoker.Tests.Games.Samples;
using TexasHoldemPoker.Framework;
using Xunit;
using Xunit.Abstractions;

namespace TexasHoldemPoker.Tests.Games.Scenarios
{
    public class GameValidationShould : Feature
    {
        public readonly GameSamples game = new GameSamples();

        public GameValidationShould(ITestOutputHelper outputHelper) : base(outputHelper)
        {
        }

        #region Validate Current Player

        [Fact]
        public void Fail_When_Player_Bets_When_It_Is_Not_Its_Turn() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Started)
                 .When(new PlayerBets(game.Id, game.Players.BigBlindId, 10_000))
                 .ThenItFailsWith<NotYourTurn>();

        [Fact]
        public void Fail_When_Player_Raises_When_It_Is_Not_Its_Turn() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Started)
                 .When(new PlayerRaises(game.Id, game.Players.BigBlindId, 10_000))
                 .ThenItFailsWith<NotYourTurn>();

        [Fact]
        public void Fail_When_Player_Goes_AllIn_When_It_Is_Not_Its_Turn() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Started)
                 .When(new PlayerGoesAllIn(game.Id, game.Players.BigBlindId))
                 .ThenItFailsWith<NotYourTurn>();

        [Fact]
        public void Fail_When_Player_Checks_When_It_Is_Not_Its_Turn() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Started)
                 .When(new PlayerChecks(game.Id, game.Players.BigBlindId))
                 .ThenItFailsWith<NotYourTurn>();

        [Fact]
        public void Fail_When_Player_Folds_When_It_Is_Not_Its_Turn() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Started)
                 .When(new PlayerFolds(game.Id, game.Players.BigBlindId))
                 .ThenItFailsWith<NotYourTurn>();

        #endregion Validate Current Player

        #region Validate Bet

        [Fact]
        public void Fail_When_Player_Bet_More_Than_It_Owns() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Happened)
                 .And(game.Flop.Started)
                 .When(new PlayerBets(game.Id, game.Players.SmallBlindId, game.Players.SmallBlind.ChipsValue + 1))
                 .ThenItFailsWith<InvalidBet>();

        [Fact]
        public void Fail_When_Player_Bet_A_Negative_Amount() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Happened)
                 .And(game.Flop.Started)
                 .When(new PlayerBets(game.Id, game.Players.SmallBlindId, -1))
                 .ThenItFailsWith<InvalidBet>();

        [Fact]
        public void Fail_When_Player_Bet_A_Null_Amount() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Happened)
                 .And(game.Flop.Started)
                 .When(new PlayerBets(game.Id, game.Players.SmallBlindId, 0))
                 .ThenItFailsWith<InvalidBet>();


        #endregion Validate Bet

        #region Validate Raise Amount

        [Fact]
        public void Fail_When_Player_Raises_When_Nobody_Bet_Before() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Happened)
                 .And(game.Flop.Started)
                 .When(new PlayerRaises(game.Id, game.Players.SmallBlindId, game.Blinds.Big * 8))
                 .ThenItFailsWith<InvalidRaise>();

        [Fact]
        public void Fail_When_Player_Raises_Less_Than_Previous_Raise() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Started)
                 .And(
                    new PlayerBet(game.Id, game.Players.UnderTheGunId, game.Blinds.Big),
                    new PlayerTurnStarted(game.Id, game.Players.DealerId),
                    new PlayerRaised(game.Id, game.Players.DealerId, game.Blinds.Big),
                    new PlayerTurnStarted(game.Id, game.Players.SmallBlindId)
                 )
                 .When(new PlayerRaises(game.Id, game.Players.SmallBlindId, game.Blinds.Big / 2))
                 .ThenItFailsWith<InvalidRaise>();

        [Fact]
        public void Fail_When_Player_Raises_A_Negative_Amount() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Started)
                 .And(
                    new PlayerBet(game.Id, game.Players.UnderTheGunId, game.Blinds.Big),
                    new PlayerTurnStarted(game.Id, game.Players.DealerId),
                    new PlayerRaised(game.Id, game.Players.DealerId, game.Blinds.Big),
                    new PlayerTurnStarted(game.Id, game.Players.SmallBlindId)
                 )
                 .When(new PlayerRaises(game.Id, game.Players.SmallBlindId, -5))
                 .ThenItFailsWith<InvalidRaise>();

        [Fact]
        public void Fail_When_Player_Raises_More_Than_Player_Owns() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Started)
                 .And(
                    new PlayerBet(game.Id, game.Players.UnderTheGunId, game.Blinds.Big),
                    new PlayerTurnStarted(game.Id, game.Players.DealerId),
                    new PlayerRaised(game.Id, game.Players.DealerId, game.Blinds.Big),
                    new PlayerTurnStarted(game.Id, game.Players.SmallBlindId)
                 )
                 .When(new PlayerRaises(game.Id, game.Players.SmallBlindId, game.Players.SmallBlind.ChipsValue))
                 .ThenItFailsWith<InvalidRaise>();

        #endregion Validate Raise Amount

        #region Validate Check

        [Fact]
        public void Fail_When_Player_Check_After_Someone_Bet() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Happened)
                 .And(game.Flop.Started)
                 .And(
                    new PlayerBet(game.Id, game.Players.SmallBlindId, game.Blinds.Big),
                    new PlayerTurnStarted(game.Id, game.Players.BigBlindId)
                 )
                 .When(new PlayerChecks(game.Id, game.Players.BigBlindId))
                 .ThenItFailsWith<InvalidCheck>();

        private History EveryBodyButBigBlindCalled()
        {
            return new History(
                new PlayerCalled(game.Id, game.Players.UnderTheGunId),
                new PlayerTurnStarted(game.Id, game.Players.DealerId),
                new PlayerFolded(game.Id, game.Players.DealerId),
                new PlayerTurnStarted(game.Id, game.Players.SmallBlindId),
                new PlayerCalled(game.Id, game.Players.SmallBlindId),
                new PlayerTurnStarted(game.Id, game.Players.BigBlindId)
                );
        }

        [Fact]
        public void Not_Fail_During_Preflop_When_BigBlind_Player_Checks_After_Everybody_Called_Or_Folded() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Started)
                 .And(EveryBodyButBigBlindCalled)
                 .When(new PlayerChecks(game.Id, game.Players.BigBlindId))
                 .ThenItDoesNotFailWith<InvalidCheck>();

        [Fact]
        public void Fail_During_Preflop_When_BigBlind_Player_Checks_But_A_Player_Has_Raised() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Started)
                 .And(
                    new PlayerCalled(game.Id, game.Players.UnderTheGunId),
                    new PlayerTurnStarted(game.Id, game.Players.DealerId),
                    new PlayerFolded(game.Id, game.Players.DealerId),
                    new PlayerTurnStarted(game.Id, game.Players.SmallBlindId),
                    new PlayerRaised(game.Id, game.Players.SmallBlindId, game.Blinds.Big),
                    new PlayerTurnStarted(game.Id, game.Players.BigBlindId))
                 .When(new PlayerChecks(game.Id, game.Players.BigBlindId))
                 .ThenItFailsWith<InvalidCheck>();

        [Fact]
        public void Fail_When_Player_Check_After_Someone_Raised() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Happened)
                 .And(game.Flop.Started)
                 .And(
                    new PlayerBet(game.Id, game.Players.SmallBlindId, game.Blinds.Big),
                    new PlayerTurnStarted(game.Id, game.Players.BigBlindId),
                    new PlayerRaised(game.Id, game.Players.BigBlindId, game.Blinds.Big),
                    new PlayerTurnStarted(game.Id, game.Players.UnderTheGunId)
                 )
                 .When(new PlayerChecks(game.Id, game.Players.UnderTheGunId))
                 .ThenItFailsWith<InvalidCheck>();

        #endregion Validate Check

        #region Validate Call

        [Fact]
        public void Fail_When_Player_Calls_When_Nobody_Bet_Before() =>
            Given.A<Game>()
                 .WithHistory(game.Started)
                 .And(game.Preflop.Happened)
                 .And(game.Flop.Started)
                 .When(new PlayerCalls(game.Id, game.Players.SmallBlindId))
                 .ThenItFailsWith<InvalidCall>();

        #endregion Validate Call
    }
}