﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using TexasHoldemPoker.Tests.Framework;
using TexasHoldemPoker.Tests.Framework.ScenarioGeneration;
using Xunit.Abstractions;

namespace TexasHoldemPoker.Tests.Games
{
    public class Feature
    {
        public Given Given { get; }

        private string FeatureName => GetType().Name.Replace("Should", "");

        public Feature(ITestOutputHelper outputHelper)
        {
            var exporters = new List<IExportAScenario>
            {
                new TextScenarioExporter(outputHelper.WriteLine)
            };

            var featureFilesPath = Environment.GetEnvironmentVariable("FEATURE_FILES_PATH", EnvironmentVariableTarget.Process);

            if (!string.IsNullOrEmpty(featureFilesPath))
            {

                var exportToFeatureFile = new TextScenarioExporter(line =>
                {
                    string clean(string path)
                    {
                        return path.EndsWith("/") || path.EndsWith(@"\") ? path : path + "/";
                    }

                    if (!Directory.Exists(featureFilesPath))
                    {
                        Directory.CreateDirectory(featureFilesPath);
                    }

                    using (var writer = new StreamWriter($"{clean(featureFilesPath)}{FeatureName}.feature", true))
                    {
                        writer.WriteLine(line);
                    }
                });

                exporters.Add(exportToFeatureFile);
            }

            Given = new Given(
                () => new StackTrace()
                        .FindCallingTestMethod()
                        .NameAsReadableSentence(),
                exporters.ToArray()
            );
        }
    }
}