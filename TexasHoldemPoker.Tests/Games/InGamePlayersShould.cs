﻿using System.Linq;
using NFluent;
using TexasHoldemPoker.Games.Domain;
using TexasHoldemPoker.Games.Domain.BettingRounds;
using TexasHoldemPoker.Players;
using Xunit;

namespace TexasHoldemPoker.Tests.Games
{
    public class InGamePlayersShould
    {
        private readonly InGamePlayer john;
        private readonly InGamePlayer jane;
        private readonly InGamePlayer jack;
        private readonly InGamePlayers players;

        public InGamePlayersShould()
        {
            john = new InGamePlayer(PlayerId.New("JOHN"), 10);
            jane = new InGamePlayer(PlayerId.New("JANE"), 20);
            jack = new InGamePlayer(PlayerId.New("JACK"), 30);

            players = new InGamePlayers(
                new[] { john, jane, jack });
        }

        [Fact]
        public void Return_The_Current_Player()
        {
            players.CurrentId = john.Id;

            Check.That(players.CurrentId).IsEqualTo(john.Id);
        }

        [Fact]
        public void Return_The_Next_Player()
        {
            players.CurrentId = john.Id;

            Check.That(players.NextId).IsEqualTo(jane.Id);
        }

        [Fact]
        public void Return_The_Player_After()
        {
            Check.That(players.After(jane.Id)).IsEqualTo(jack.Id);
        }

        [Fact]
        public void Return_The_Count_Of_Players()
        {
            Check.That(players.Count).IsEqualTo(3);
        }

        [Fact]
        public void Return_The_Next_Player_At_The_End_Of_The_List_That_Is_The_First_Player_Of_The_List()
        {
            players.CurrentId = jack.Id;

            Check.That(players.NextId).IsEqualTo(john.Id);
        }

        [Fact]
        public void Skip_The_Next_Folded_Player()
        {
            jane.Fold(BettingRoundName.None);

            players.CurrentId = john.Id;

            Check.That(players.NextId).IsEqualTo(jack.Id);
        }

        [Fact]
        public void Skip_The_Next_AllIn_Player()
        {
            jane.AllIn(BettingRoundName.None);

            players.CurrentId = john.Id;

            Check.That(players.NextId).IsEqualTo(jack.Id);
        }


        [Fact]
        public void Be_Enumerable()
        {
            var playerList = players.ToList();

            Check.That(playerList).ContainsExactly(
                john.Id, jane.Id, jack.Id);
        }


        [Fact]
        public void Enumerate_Players_Starting_After()
        {
            var playerList = players.StartsAfter(jane.Id).ToList();

            Check.That(playerList).ContainsExactly(
                jack.Id, john.Id, jane.Id);
        }
    }
}