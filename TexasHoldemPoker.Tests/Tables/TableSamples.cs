﻿using TexasHoldemPoker.Players;
using TexasHoldemPoker.Tables;

namespace TexasHoldemPoker.Tests.Tables
{
    public class TableSamples
    {
        public readonly TableId Id = TableId.New("TABLE");

        public readonly decimal SmallBlind = 50;
        public readonly decimal BigBlind = 100;

        public readonly int HeadsUpGameNumberOfSeats = 2;

        public readonly PlayerId PlayerOne = PlayerId.New("[ONE ]");
        public readonly decimal PlayerOneStake = 100_000;

        public readonly PlayerId PlayerTwo = PlayerId.New("[TWO ]");
        public readonly decimal PlayerTwoStake = 150_000;

        public readonly PlayerId LatePlayer = PlayerId.New("[LATE]");
        public readonly decimal LatePlayerStake = 1_000_000;
    }

}