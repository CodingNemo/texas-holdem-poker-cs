﻿using System;
using System.Linq;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Framework
{
    public class Aggregate
    {
        private ILogEvents log;
        private StateProjection state;

        public Aggregate(
            History history, 
            ILogEvents log, 
            StateProjection state)
        {
            this.log = log;
            this.state = state;
            state.Replay(history);
        }
        
        protected TState Get<TState>() where TState : StateProjection => (TState)state; 

        protected void Fire(DomainEvent domainEvent)
        {
            log.Add(domainEvent);
            state.Apply(domainEvent);
        }

        public static TAggregate Create<TAggregate>(ILogEvents logEvents, History history = null) 
            where TAggregate : Aggregate
        {
            var constructor = typeof(TAggregate).GetConstructors().Where(c =>
            {
                var constructorParameters = c.GetParameters();
                if (constructorParameters.Length != 2) return false;

                return typeof(History).IsAssignableFrom(constructorParameters[0].ParameterType)
                       && constructorParameters[1].ParameterType == typeof(ILogEvents);
            }).FirstOrDefault();

            if (constructor == null)
            {
                throw new InvalidOperationException(
                    $"Can not find a constructor for {nameof(TAggregate)} with Parameters ({nameof(History)}, {nameof(ILogEvents)}");

            }

            return (TAggregate)constructor.Invoke(new object[] { history ?? History.Clean, logEvents });
        }
    }
}
