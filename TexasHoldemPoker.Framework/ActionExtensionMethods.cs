using System;
using static System.Linq.Enumerable;

public static class ActionExtensionMethods
{
    public static void Repeat(int times, Action action)
    {
        Range(0, times).ForEach(_ => action());
    }
}