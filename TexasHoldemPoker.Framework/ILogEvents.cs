﻿namespace TexasHoldemPoker.Framework
{
    public interface ILogEvents
    {
        void Add(params DomainEvent[] domainEvents);
    }

    public interface ILoadHistory
    {
        History HistoryOf(object aggregateId);
    }
}