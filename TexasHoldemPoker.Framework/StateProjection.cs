﻿using System;
using System.Collections.Generic;

namespace TexasHoldemPoker.Framework
{
    public abstract class StateProjection
    {
        private readonly Dictionary<Type, Action<DomainEvent>> eventHandlers =
            new Dictionary<Type, Action<DomainEvent>>();

        protected void Register<TDomainEvent>(Action<TDomainEvent> onEvent)
            where TDomainEvent : DomainEvent
        {
            if (eventHandlers.ContainsKey(typeof(TDomainEvent)))
            {
                throw new InvalidOperationException(
                    $"{typeof(TDomainEvent).Name} is already handled by {GetType().Name}");
            }
            eventHandlers[typeof(TDomainEvent)] = e => onEvent((TDomainEvent) e);
        }

        public void Apply(DomainEvent @event)
        {
            if (!eventHandlers.ContainsKey(@event.GetType()))
            {
                return;
            }

            eventHandlers[@event.GetType()](@event);
        }

        public void Replay(IEnumerable<DomainEvent> events)
        {
            foreach (var @event in events)
            {
                Apply(@event);
            }
        }
    }
}