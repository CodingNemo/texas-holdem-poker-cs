﻿using System.Collections.Generic;
using System.Linq;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Tests.Framework
{
    public class InMemoryEventStore : ILogEvents, ILoadHistory
    {
        private readonly Dictionary<object, List<DomainEvent>> historyByAggregate =
            new Dictionary<object, List<DomainEvent>>();

        public void Add(params DomainEvent[] domainEvents)
        {
            foreach (var @event in domainEvents)
            {
                Log(@event);
            }
        }

        private void Log(DomainEvent @event)
        {
            if (!historyByAggregate.ContainsKey(@event.AggregateId))
            {
                historyByAggregate.Add(@event.AggregateId, new List<DomainEvent>());
            }

            historyByAggregate[@event.AggregateId].Add(@event);
        }

        public History HistoryOf(object aggregateId)
        {
            if (!historyByAggregate.Any())
            {
                return History.Clean;
            }

            if (aggregateId == null)
            {
                aggregateId = historyByAggregate.Keys.First();
            }

            if (!historyByAggregate.TryGetValue(aggregateId, out var history))
            {
                return History.Clean;
            }

            return new History(history);
        }
    }
}