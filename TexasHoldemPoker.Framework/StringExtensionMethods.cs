﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TexasHoldemPoker.Framework
{
    public static class StringExtensionMethods
    {
        public static IEnumerable<string> SplitOnUpperCaseLetters(this string text)
        {
            List<char> letters = new List<char>();
            letters.Add(char.ToUpper(text[0]));

            foreach (var c in text.Skip(1))
            {
                if (char.IsUpper(c))
                {
                    yield return new string(letters.ToArray());
                    letters.Clear();
                    letters.Add(char.ToLower(c));
                }
                else
                {
                    letters.Add(c);
                }
            }

            yield return new string(letters.ToArray());
        }

        public static string Capitalize(this string text)
        {
            var builder = new StringBuilder();

            var firstCapital = char.ToUpper(text.First());
            builder.Append(firstCapital);

            text.Skip(1).ForEach(c => builder.Append(char.ToLower(c)));

            return builder.ToString();
        }
    }
}
