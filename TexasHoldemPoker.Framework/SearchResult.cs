﻿using System;

namespace TexasHoldemPoker.Framework
{
    public abstract class SearchResult<T>
    {
        private class FoundResult : SearchResult<T>
        {
            private readonly T found;
            public FoundResult(T found)
            {
                this.found = found;
            }

            public override SearchResult<T> OnFound(Action<T> onFound)
            {
                onFound(found);
                return this;
            }

            public override SearchResult<T> OnNotFound(Action onNotFound)
            {
                return this;
            }
        }

        private class NotFoundResult : SearchResult<T>
        {
            public override SearchResult<T> OnFound(Action<T> onFound)
            {
                return this;
            }

            public override SearchResult<T> OnNotFound(Action onNotFound)
            {
                onNotFound();
                return this;
            }
        }

        public abstract SearchResult<T> OnFound(Action<T> onFound);
        public abstract SearchResult<T> OnNotFound(Action onNotFound);

        public static SearchResult<T> Found(T found)
        {
            return new FoundResult(found);
        }

        public static SearchResult<T> NotFound { get; } = new NotFoundResult();
        
    }
}