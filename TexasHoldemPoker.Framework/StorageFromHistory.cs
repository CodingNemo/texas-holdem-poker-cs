﻿using System.Linq;
using TexasHoldemPoker.Framework;

namespace TexasHoldemPoker.Tests.Framework
{
    public class StorageFromHistory : Storage
    {
        private readonly ILoadHistory loadHistory;
        private readonly ILogEvents logEvents;

        public StorageFromHistory(
            ILoadHistory loadHistory,
            ILogEvents logEvents)
        {
            this.loadHistory = loadHistory;
            this.logEvents = logEvents;
        }

        public T Create<T>() where T : Aggregate
        {
            return Aggregate.Create<T>(logEvents);
        }

        public SearchResult<T> SearchById<T>(object id)
            where T : Aggregate
        {
            var history = loadHistory.HistoryOf(id);

            if(history.Any())
            {
                return SearchResult<T>.Found(Aggregate.Create<T>(logEvents, history));
            }

            return SearchResult<T>.NotFound;
            
        }
    }
}